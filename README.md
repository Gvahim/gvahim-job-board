Installation
1. Install docker & docker-compose  - min 2 gb ram
2. docker-compose up -d db  - will start database
4. to start development of client
docker-compose up -d client-dev
5. to start development of node-crawler:
docker-compose up -d chrome
docker-compose exec chrome sh  
node crawler.js
// be carefull. chromewebdriver crawler is in the folder chrome
6. to start python crawler
docker-compose up -d crawler
