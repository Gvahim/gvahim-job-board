# Changelog
This file contain all notable and important changes of this service.

## [Unreleased]

### Add
- Parsing websites
- Integration with MongoDB
- REST API interface
- Notifications

## [0.2.0] - 

### Add
- Add dependencies: Flask-SQLAlchemy, psycopg2-binary

### Fix
### Remove
### Change

## [0.1.0] - 2019-10-28

### Add
- Create test Resource with test json in root path
- Create docker-compose.yml
- Create Dockerfile for crawler
- Create structure for tests
- Add follow dependencies: Flask, Flask-RESTful, Flask-Testing, coverage, flake8, black
- Create structure of project