# services/crawler/project/request/request.py

import functools
import requests
from project.request.proxy.proxy_provider import ProxyProvider
from project.request.agent.agent_provider import AgentProvider
from project.request.proxy.proxy_provider_file import ProxyProviderFile
from project.request.agent.agent_provider_fake import AgentProviderFake


def agent_proxy(agent_provider: AgentProvider, proxy_provider: ProxyProvider):
    def decorator(fn):
        @functools.wraps(fn)
        def wrapper(method, url):
            # TODO add work with *args **kwargs
            """proxy = proxy_provider.get_proxy()
            proxies = {
                'http': proxy,
                'https': proxy,
            }"""
            headers = {
                'User-Agent': agent_provider.get_agent()
            }
            return fn(method, url, headers=headers)
        return wrapper
    return decorator


requests.request = agent_proxy(AgentProviderFake(), ProxyProviderFile())(requests.request)