# services/crawler/project/request/proxy/proxy_provider_file.py
import random
from .proxy_provider import ProxyProvider


class ProxyProviderFile(ProxyProvider):

    def get_proxy(self) -> str:
        proxies = []
        with open('./project/request/proxy/proxy.txt', 'r') as reader:
            proxies = list(reader.readlines())
        proxies = list(map(lambda s: s.strip(), proxies))
        return proxies[random.randint(0, len(proxies)-1)]
