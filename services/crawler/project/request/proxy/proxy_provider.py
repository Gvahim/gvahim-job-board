# services/crawler/project/request/proxy/proxy_provider.py

from abc import ABC, abstractmethod


class ProxyProvider(ABC):

    @abstractmethod
    def get_proxy(self) -> str:
        pass
