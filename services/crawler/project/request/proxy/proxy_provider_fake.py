# services/crawler/project/request/proxy/proxy_provider_fake.py

import asyncio
from .proxy_provider import ProxyProvider
from proxybroker import Broker


class ProxyProviderFake(ProxyProvider):

    async def __save(self, proxies, filename):
        """Save proxies to a file."""
        with open(filename, 'w') as f:
            while True:
                proxy = await proxies.get()
                if proxy is None:
                    break
                proto = 'https' if 'HTTPS' in proxy.types else 'http'
                row = '%s://%s:%d\n' % (proto, proxy.host, proxy.port)
                f.write(row)

    def get_proxy(self) -> str:
        proxies = asyncio.Queue()
        broker = Broker(proxies)
        tasks = asyncio.gather(broker.find(types=['HTTP', 'HTTPS'], limit=10),
                               self.__save(proxies, filename='proxies.txt'))
        loop = asyncio.get_event_loop()
        loop.run_until_complete(tasks)
        return ''
