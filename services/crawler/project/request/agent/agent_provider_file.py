# services/crawler/project/request/proxy/proxy_provider_file.py
import random
from .agent_provider import AgentProvider


class AgentProviderFile(AgentProvider):

    def get_agent(self) -> str:
        agents = []
        with open('./project/request/agent/agent.txt', 'r') as reader:
            agents = list(reader.readlines())
        agents = list(map(lambda s: s.strip(), agents))
        return agents[random.randint(0, len(agents)-1)]
