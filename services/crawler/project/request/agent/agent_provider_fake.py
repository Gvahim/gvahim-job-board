# services/crawler/project/request/proxy/proxy_provider_fake.py

from .agent_provider import AgentProvider
from fake_useragent import UserAgent


class AgentProviderFake(AgentProvider):

    def get_agent(self) -> str:
        ua = UserAgent()
        return ua.random
