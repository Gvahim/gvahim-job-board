# services/crawler/project/request/agent/agent_provider.py

from abc import ABC, abstractmethod


class AgentProvider(ABC):
    @abstractmethod
    def get_agent(self) -> str:
        pass
