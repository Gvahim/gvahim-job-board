# services/crawler/project/crawler/strategy/amazon.py

import json
from .strategy import Strategy


class Amazon(Strategy):
    """Class provide strategy to parse web site"""

    def get_id(self) -> str:
        return 'amazon'

    def get_pages(self) -> list:
        url = self.get_url()
        url.set_path('/en/search.json')
        query = {
            'country': 'ISR',
            'facets[]': [
                'location',
                'business_category',
                'category',
                'schedule_type_id',
                'employee_class',
                'normalized_location',
                'job_function_id'
            ],
            'loc_query': 'Israel',
            'offset': 0,
            'radius': '24km',
            'result_limit': 10,
            'sort': 'relevant'
        }
        url.add_query(query)
        return [
            url
        ]

    def parse(self, url) -> list:
        response = self._requests.request('GET', url)
        data = json.loads(response.text)
        results = []
        for item in data['jobs']:
            results.append({
                'title': item['title'],
                'company_name': item['company_name'],
                'job_url': item['job_path'],
                'job_location': item['location'],
                'description': item['description'],
            })
        return results
