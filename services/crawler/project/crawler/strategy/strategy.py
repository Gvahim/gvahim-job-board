# services/crawler/project/crawler/strategy/strategy.py

import json
from abc import ABC, abstractmethod
from project.request.request import requests
from project.url.Url import Url
import copy


class Strategy(ABC):
    """Abstract class provide structure to implementation strategies"""

    def __init__(self, url: str):
        self.__url = Url(url)
        self._requests = requests

    def execute(self) -> dict:
        results = {
            'website_id': self.get_id(),
            'json_data': [],
            'version': ''
        }
        pages = self.get_pages()

        if not all(isinstance(page, Url) for page in pages):
            raise ValueError("Method get_pages from strategy: '{}' should return list "
                             "that contains just Url objects".format(self.get_id()))

        for url in pages:
            result = self.parse(url.get_url())
            results['json_data'].append(result)

        results['json_data'] = json.dumps(results['json_data'][0])
        return results

    def get_url(self):
        return copy.deepcopy(self.__url)

    @abstractmethod
    def get_id(self) -> str:
        """Function should return strategy's id as string"""
        pass

    @abstractmethod
    def get_pages(self) -> list:
        """Function should provide logic to return list of links with pages."""
        pass

    @abstractmethod
    def parse(self, html) -> list:
        """Function should provide logic to parse data of pages."""
        pass
