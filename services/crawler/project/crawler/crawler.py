# services/crawler/project/crawler/crawler.py

from .strategy.strategy import Strategy


class Crawler:
    """Class provide core functionality for crawler. This class get all strategies and execute them

    Attributes:
        __strategies (dict): Contain all strategies
    """
    __strategies = {}

    def __init__(self):
        pass

    def get_strategy(self, strategy_id):
        """get list of strategies"""
        if strategy_id in self.__strategies:
            return self.__strategies[strategy_id]
        return None

    def get_strategies(self) -> dict:
        """get dict of strategies"""
        return self.__strategies

    def add_strategy(self, strategy) -> 'Crawler':
        """add strategy to list. Provide fluent interface"""
        if strategy.get_id() in self.__strategies:
            """@TODO Log adding strategy with the same id"""
            return self
        self.__strategies.update({strategy.get_id(): strategy})
        return self

    def run(self) -> dict:
        """function run all strategies"""
        if not self.__strategies:
            return False

        for id, strategy in self.__strategies.items():
            yield strategy.execute()
