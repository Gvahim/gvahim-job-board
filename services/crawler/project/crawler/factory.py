# services/crawler/project/crawler/factory.py

from .crawler import Crawler
from .strategy.amazon import Amazon
from project.url.Url import Url


class Factory:
    """Class provide pattern Factory and create Crawler with all configuration"""
    @staticmethod
    def create() -> Crawler:
        crawler = Crawler()
        # add amazon
        crawler.add_strategy(Amazon('https://amazon.jobs'))
        return crawler
