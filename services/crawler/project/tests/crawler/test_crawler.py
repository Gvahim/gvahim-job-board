# services/crawler/project/tests/test_crawler.py

import unittest
from project.tests.base import BaseTestCase
from project.crawler.crawler import Crawler
from project.crawler.strategy.amazon import Amazon


class TestCrawler(BaseTestCase):
    def test_get_strategy(self):
        """test get strategy by id"""
        crawler = Crawler()
        amazon = Amazon('https://www.amazon.jobs')
        crawler.add_strategy(amazon)
        strategy = crawler.get_strategy(amazon.get_id())
        self.assertIsInstance(strategy, Amazon)

    def test_get_all_strategies(self):
        """test get all strategies in crawler"""
        crawler = Crawler()
        amazon = Amazon('https://www.amazon.jobs')
        crawler.add_strategy(amazon)
        strategies = crawler.get_strategies()
        self.assertIsInstance(strategies, dict)
        self.assertEqual(len(strategies), 1)

    def test_add_strategy(self):
        """test adding of strategy to crawler"""
        crawler = Crawler()
        amazon = Amazon('https://www.amazon.jobs')
        crawler.add_strategy(amazon)
        strategy = crawler.get_strategy(amazon.get_id())
        self.assertIsInstance(strategy, Amazon)

    def test_uniques_strategy(self):
        """test unique adding of strategy to crawler"""
        crawler = Crawler()
        amazon1 = Amazon('https://www.amazon.jobs')
        amazon2 = Amazon('https://www.amazon.jobs')
        crawler.add_strategy(amazon1)
        crawler.add_strategy(amazon2)
        strategies = crawler.get_strategies()
        self.assertEqual(len(strategies), 1)


if __name__ == "__main__":
    unittest.main()