# services/crawler/project/tests/test_crawler.py

import unittest
from project.tests.base import BaseTestCase
from project.crawler.factory import Factory
from project.crawler.crawler import Crawler


class TestFactory(BaseTestCase):
    def test_create(self):
        # test creating of Crawler
        crawler = Factory.create()
        self.assertIsInstance(crawler, Crawler)
        pass


if __name__ == "__main__":
    unittest.main()