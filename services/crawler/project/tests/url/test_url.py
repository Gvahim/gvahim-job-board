# services/crawler/project/tests/url/test_url.py

import unittest
from project.url.Url import Url
from project.tests.base import BaseTestCase


class TestUrl(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.__str='https://amazon.jobs'
        self.__url = Url(self.__str)

    def test_get_url(self):
        """test return of string url"""
        self.assertEqual(self.__url.get_url(), self.__str)

    def test_set_path(self):
        """test setting path url"""
        self.__url.set_path('/en/search.json')
        self.assertEqual(self.__url.get_url(), self.__str + '/en/search.json')

    def test_get_path(self):
        """test getting path url"""
        self.__url.set_path('/en/search.json')
        self.assertEqual(self.__url.get_path(), '/en/search.json')

    def test_url_valid_success(self):
        """test valid url"""
        valid_urls = [
            'ftp://amazon',
            'http://amazon',
            'https://amazon',
            'https://amazon.jobs',
            'https://amazon.jobs/en/search.json'
        ]
        for result in map(Url.valid, valid_urls):
            self.assertTrue(result)

    def test_url_valid_fail(self):
        """test invalid url"""
        invalid_urls = [
            'https://',
            '1234',
            'sdgsdfgsdfg'
        ]
        for result in map(Url.valid, invalid_urls):
            self.assertFalse(result)

    def test_get_query(self):
        """test getting path url"""
        query = {
            'get_param1': 'one',
            'get_param2': 'two',
            'array_param[]': [
                'one',
                'two'
            ]
        }

        self.__url.add_query(query)
        self.assertEqual(self.__url.get_query(), query)

    def test_add_query(self):
        """test adding query to url"""
        query1 = {
            'get_param1': 'one',
            'get_param2': 'two',
            'array_param[]': [
                'one',
                'two'
            ]
        }

        self.__url.add_query(query1)
        self.assertEqual(self.__url.get_url(), self.__str +
                         '?get_param1=one&get_param2=two&array_param%5B%5D=one&array_param%5B%5D=two')

        query2 = {
            'get_param1': 'one',
            'get_param2': 'two_rewrite',
            'get_param3': 'three',
        }

        self.__url.add_query(query2)
        self.assertEqual(self.__url.get_url(), self.__str +
                         '?get_param1=one&get_param2=two_rewrite'
                         '&array_param%5B%5D=one&array_param%5B%5D=two&get_param3=three')


if __name__ == "__main__":
    unittest.main()