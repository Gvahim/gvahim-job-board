# services/crawler/project/crawler/url.py

import copy
from urllib.parse import urlparse, urlunparse, urlencode


class Url:

    def __init__(self, url: str):
        if not self.valid(url):
            raise ValueError('Url: {} has invalid format'.format(url))
        self.__base_url = list(urlparse(url))

    def set_path(self, path: str) -> 'Url':
        self.__base_url[2] = path
        return self

    def get_path(self):
        return self.__base_url[2]

    def add_query(self, query: dict) -> 'Url':
        if not self.__base_url[4]:
            self.__base_url[4] = {}
        self.__base_url[4].update(query)
        return self

    def get_query(self) -> dict:
        return self.__base_url[4]

    def get_url(self):
        url = copy.deepcopy(self.__base_url)
        url[4] = urlencode(url[4], True)
        return urlunparse(url)

    @staticmethod
    def valid(url):
        try:
            result = urlparse(url)
            return all([result.scheme, result.netloc])
        except ValueError:
            return False
