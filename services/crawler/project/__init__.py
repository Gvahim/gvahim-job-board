# services/crawler/project/__init__.py

import os
from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

app.config.from_object(os.getenv("APP_SETTINGS"))


class TestResource(Resource):
    def get(self):
        return {"status": "success", "content": "hi!"}


api.add_resource(TestResource, "/")
