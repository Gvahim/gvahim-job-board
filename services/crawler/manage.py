# services/crawler/manage.py

import sys
import unittest
import psycopg2
from flask.cli import FlaskGroup
from project import app
from project.crawler.factory import Factory

cli = FlaskGroup(app)


@cli.command()
def test():
    """Provide functionality to run all tests"""
    tests = unittest.TestLoader().discover('project/tests', pattern='test*.py', )
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    sys.exit(result)


@cli.command("start_crawler")
def start_crawler():
    """Provide functionality to run crawler"""
    conn = psycopg2.connect(dbname='jobboard_gvahim', user='test',
                            password='test', host='db')
    cursor = conn.cursor()

    crawler = Factory.create()
    for item in crawler.run():
        """cursor.execute("SELECT version FROM crawler.parse_data WHERE website_id = %(website_id)s ORDER BY version DESC",
            {'website_id': item['website_id']})
        version = cursor.fetchone()
        if version:
            version = version[0]
            version += 1
        else:
            version = 1
        item['version'] = version

        cursor.execute('INSERT INTO crawler.parse_data (website_id, json_data, version) VALUES (%(website_id)s, %(json_data)s, %(version)s)', item)
        conn.commit()"""

    cursor.close()
    conn.close()


if __name__ == '__main__':
    cli()

