# How to run crawler:
In order to run crawler you should run docker-compose and execute console command:

``docker-compose exec crawler python manage.py start_crawler``

JSON column in DB contain follow fields:

`title`

`company_name`

`job_url`

`job_location`

`description`
 