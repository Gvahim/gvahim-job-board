const {connectToDb} = require('./db.js');

let {protostrategy} =require('./strategies');
const puppeteer = require('puppeteer');


async function crawlWebsite(url, evFunc) {
    const browser = await puppeteer.launch({
        headless: true,
        executablePath: '/usr/bin/chromium-browser',
        args: [
            "--no-sandbox",
            "--disable-gpu",
        ]
    });
    const page = await browser.newPage();
    await page.goto(url, {timeout: 0, waitUntil: 'networkidle0',});
    const data = await page.evaluate(evFunc);
    await page.close();
    await browser.close();
    return data;
}

async function start()
{
    const connectionString = process.env.POSTGRES_LINE || 'postgres://test:test@localhost:5435/jobboard_gvahim';

   //  const db =  await connectToDb(connectionString);
    const res=await crawlWebsite('https://imoving.com', protostrategy);
    console.log(res);
    process.exit();
}
start();
