const {Client} = require('pg')

const connectToDb = async (connectionString) => {
    return new Promise((ok, nok) => {
        console.log('connecting', connectionString);
        const client = new Client({connectionString});

        client.connect((err) => {
            if (err) {
                console.error('connection error', err.stack)
                nok('no db connection');
            } else {
                console.log('db connected')
                ok(client);
            }
        })

    })
}



module.exports={Client,connectToDb}
