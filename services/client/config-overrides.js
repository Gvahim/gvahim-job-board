const {override, fixBabelImports, addLessLoader} = require('customize-cra');

module.exports = override(
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true,

    }),
    addLessLoader({
        javascriptEnabled: true,
        modifyVars: {
            '@primary-color': '#da7443',
            '@btn-primary-bg':'#9e1202',
            //  '@menu-dark-selected-item-text-color':'#4a235a',
            // '@table-selected-row-bg':'#0a235a',
            '@menu-highlight-color':'#666666',
            /*'@menu-highlight-background-color':'#9a235a',
            '@menu-background-color':'#fffa5a',
            '@menu-selected-background-color':'#eeea5a',
            '@menu-dark-highlight-color':'#999999',
            '@menu-item-selected-color':'#999999',
            '@menu-item-selected':'#999999',
            '@btn-primary-color':'#fff'*/
        },
    }),
);
