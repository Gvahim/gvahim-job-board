const {Client} = require('pg')
const fetch = require('node-fetch');
const Bluebird = require('bluebird');

fetch.Promise = Bluebird;
const connectToDb = async (connectionString) => {
    return new Promise((ok, nok) => {
        console.log('connecting', connectionString);
        const client = new Client({connectionString});

        client.connect((err) => {
            if (err) {
                console.error('connection error', err.stack)
                nok('no db connection');
            } else {
                console.log('db connected')
                ok(client);
            }
        })

    })
}
const connectionString = process.env.POSTGRES_LINE || 'postgres://test:test@localhost:5435/jobboard_gvahim';
const start = async () => {
    try {
        const db = await connectToDb(connectionString);
        //fetch('https://github.com/')
        const answer=await fetch('https://amazon.jobs/en/search.json?base_query=&city=&country=ISR&county=&facets%5B%5D=location&facets%5B%5D=business_category&facets%5B%5D=category&facets%5B%5D=schedule_type_id&facets%5B%5D=employee_class&facets%5B%5D=normalized_location&facets%5B%5D=job_function_id&latitude=&loc_group_id=&loc_query=Tel%27+Aviv%2C+Israel&longitude=&offset=0&query_options=&radius=24km&region=Tel+Aviv&result_limit=1000&sort=relevant');
        const list=await answer.json();
        const website_id=1;
        await db.query('delete from crawler.parse_data');

        const sqv="select max(version) as version from crawler.parse_data where website_id='"+website_id+"'";
        const res0=await db.query(sqv);
        let version=res0.rows[0].version;
        // console.log(version);
        version++;

        // const version=1;
        var line=JSON.stringify(list.jobs);
        line=line.replace(/'/g, "''")

        const sq=`INSERT INTO crawler.parse_data (website_id, json_data, version) VALUES (${website_id}, '${line}', ${version})`;
        //console.log(sq);
        const res=await db.query(sq);
        console.log(res.rowCount);
        process.exit();

        //console.log('l', await list.text());
        // .then(res => res.text())
        // .then(body => console.log(body));
    }
    catch(ex)
    {
        console.log(ex);
    }
}
start();
