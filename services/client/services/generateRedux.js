//const connectionString =/* process.env.POSTGRES_LINE ||*/ 'postgres://test:test@db:5432/jobboard_gvahim';
// const connectionString = /*process.env.POSTGRES_LINE ||*/ 'postgres://test:test@gvahim-job-board_db_1:5435/jobboard_gvahim';
const connectionString = /*process.env.POSTGRES_LINE ||*/ 'postgres://test:test@0.0.0.0:5435/jobboard_gvahim';
console.log('NET', connectionString);
if (connectionString==='') {
    console.log('NET - NO DB CONNECTION');
    process.exit(-1);

}
const fs=require("fs");

class Services {
    constructor(db) {
        this.db = db;

    }
    async init() {
       // console.log('async init');
    }
    async getLatestJob() {
        /*let sql = `SELECT DISTINCT ON (website_id) version,
                   website_id,
                   id,
                   json_data->>'CompanyName' as CompanyName,
                   json_data->>'Title' as Title,
                   json_data->>'JobLocation' as JobLocation,
                   json_data->>'JobUrl' as JobUrl,
                   json_data->>'description' as description
                   FROM crawler.parse_data
                   ORDER BY website_id, version desc`;*/
        let result2 = await this.db.query('select * from crawler.parse_data');
        //console.log('rr',result2.rows);


        const sql=`SELECT DISTINCT ON (website_id) version, website_id,id,
                   json_array_length(json_data),
                   json_array_elements(json_data)->'title' as Title,
                   json_array_elements(json_data)->'job_url' as JobUrl,
                   json_array_elements(json_data)->'company_name' as CompanyName,
                   json_array_elements(json_data)->'job_location' as JobLocation,
                   json_array_elements(json_data)->'description' as description
FROM crawler.parse_data
ORDER BY website_id, version desc`;

        let result = await this.db.query(sql);
        console.log('faa++',sql);

        if (result) return result.rows; else return false;
    }

    async generateReduxFile()
    {

        let jobs=await this.getLatestJob();

        console.log(jobs);
        let redux={data:{rt:19}, jobs}
        return redux;
    }
}

const {Client} = require('pg')

const connectToDb = async (connectionString) => {
    return new Promise((ok, nok) => {
        console.log('connecting', connectionString);
        const client = new Client({connectionString});

        client.connect((err) => {
            if (err) {
                console.error('connection error', err.stack)
                nok('no db connection');
            } else {
                console.log('db connected')
                ok(client);
            }
        })

    })
}

const start=async()=>{
    const db=await connectToDb(connectionString);
    const services = new Services(db);
    console.log('+++aaa');

    const jobs=await services.generateReduxFile();
    console.log('j+++', jobs);
    const toWrite="   window.__PRELOADED_STATE__ ="+JSON.stringify(jobs);
    const rand=Math.round(Math.random(new Date().getTime())*1000000)

    fs.writeFileSync(`public/redux${rand}.js`, toWrite);
    let fl=fs.readFileSync('public/index_proto.html').toString();
    fl=fl.replace('<script src="http://localhost:7000/jobs"></script>', `<script src="%PUBLIC_URL%/redux${rand}.js"></script>`);
    console.log(fl);
    fs.writeFileSync('public/index.html', fl);

   // console.log(res);
    process.exit();


}

start();
