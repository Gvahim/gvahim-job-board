import firebase from 'firebase';
import React from "react";

export const APP_STARTED = 'APP_STARTED';
export const CREATE_DICTIONARIES = 'CREATE_DICTIONARIES';

export const CLEAR_FLOAT = (<div style={{clear: "both"}}/>);

const firebaseConfigA = {
    apiKey: "AIzaSyBkmLnIkv-DlBnl0lcxvdKRXCmn1JG4nUE",
    authDomain: "test-4d1cd.firebaseapp.com",
    databaseURL: "https://test-4d1cd.firebaseio.com",
    projectId: "test-4d1cd",
    storageBucket: "test-4d1cd.appspot.com",
    messagingSenderId: "458030402487",
    appId: "1:458030402487:web:c048c835d929305742b632"
};
export const firebaseConfig =
    process.env.NODE_ENV === 'production' ? firebaseConfigA : firebaseConfigA;
firebase.initializeApp(firebaseConfig);

export {firebase};
