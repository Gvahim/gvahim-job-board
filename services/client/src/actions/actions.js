import * as act from './constants';
import {CREATE_DICTIONARIES} from './constants';

export const appStarted = () => ({
    type: act.APP_STARTED,
});

export const updateDictionaries = (dispatch) => (jobs) => dispatch({ type: CREATE_DICTIONARIES, payload: jobs });