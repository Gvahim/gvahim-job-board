import React, { Component } from 'react';
import { Menu, Icon } from 'antd';
import './App.css';
import * as act from './actions/'
import {JobList, MainApp, Auth} from './components'
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Link } from 'react-router-dom';

const { SubMenu } = Menu;

class App extends Component {
    state = {
        current: 'mail',
    };


    render() {
        return (  <Router>
                <Route path={"/"} component={MainApp} />
                <Route path={"/jobs"} component={JobList} />
                <Route path={"/auth"} component={Auth} />

            </Router>
            );




    }
}

export default App;
