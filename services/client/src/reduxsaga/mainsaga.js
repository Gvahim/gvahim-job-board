import {
    /* call,
    put,
    fork,
    race, spawn,
    ,
    select,*/
    take,
    select,
    put,
    call,
    takeEvery,
    takeLatest,
    // takeLatest,
    // effects,
    // actionChannel
} from 'redux-saga/effects'
import {eventChannel, END, delay} from 'redux-saga'
import * as act from '../actions/';
//import * as actc from '../constants/';

// import {fetchU} from '../common/helpers';
// import {contactUs} from "../actions";
//import * as selectors from "./selectors";
// const loaderArrayActions=[];
// import {router, createHashHistory, createBrowserHistory} from 'redux-saga-router';

function* StartApp(action) {
    if (action.type === act.appStarted().type) {
        // console.log('started!', new Date().getTime());
       // yield put(act.getTxts());
    }
}

function* mainSaga() {

    yield takeEvery(act.appStarted, StartApp);

    yield put(act.appStarted());
    //yield put(act.appStarted());
    // console.log('chk', act.loaderArrayActions());
}

export {mainSaga}
