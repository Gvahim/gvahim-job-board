import {CREATE_DICTIONARIES} from "../actions";

const initialState = {};

export const dictionaryReducer = (state = initialState, {type, payload}) => {
    if (type !== CREATE_DICTIONARIES) {
        return state;
    }

    const companies = new Set();
    const locations = new Set();

    payload.forEach((job) => {
        locations.add(job.joblocation);
        companies.add(job.companyname);
    });

    const setToSortedArr = (set) => Array.from(set).sort();

    return {
        locations: setToSortedArr(locations),
        companies: setToSortedArr(companies)
    };
};