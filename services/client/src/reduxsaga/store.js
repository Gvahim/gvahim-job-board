import { composeWithDevTools } from 'redux-devtools-extension';
import {createStore, applyMiddleware, compose} from 'redux';
// import thunk from 'redux-thunk';

import reducer from "./mainreducer";
// import rootReducer from "../../client/src/reduxsaga";
import {mainSaga} from "./mainsaga";
import createSagaMiddleware from 'redux-saga'

// const store = createStore(reducer, composeWithDevTools());
const initialState = window.__PRELOADED_STATE__;

// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__
// const initialState = {};

const sagaMiddleware = createSagaMiddleware()

const middleware = applyMiddleware(...[sagaMiddleware/*, thunk*/]);

const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    reducer,
    initialState,
    composeEnhancers(middleware)
);

sagaMiddleware.run(mainSaga)
export default store;
