import {routerReducer} from 'react-router-redux'
import {combineReducers} from 'redux'
import dataReducer from './datareducer'
import jobs from './jobsreducer'
import {dictionaryReducer} from "./dictionary-reducer";

export default combineReducers({
    routing: routerReducer,
    jobs,
    data: dataReducer,
    dictionaries: dictionaryReducer
})

