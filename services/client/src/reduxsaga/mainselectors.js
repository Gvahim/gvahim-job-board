export const ifAppStarted = (state) => (state.data ? state.data.started : false);
export const AllJobs = (state) => (state.jobs ? state.jobs : []);
