import * as act from '../actions/'

const initialState = {};

export default (state = initialState, {type, payload}) => {
    console.log(state);

    switch (type) {

        case act.APP_STARTED: {
            return Object.assign({}, state, {
                started: true
            });

        }
        default:
            return state
    }
}
