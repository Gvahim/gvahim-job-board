import React, {Component} from 'react';
import {Avatar, Button, Collapse, Layout} from 'antd';
import "../../styles/main.css"
import {FaMapMarker} from "react-icons/all";
import {CLEAR_FLOAT} from "../../actions";
import ReactPaginate from 'react-paginate';

const {Panel} = Collapse;

const PER_PAGE = 10;

export class JobList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            jobsToShow: props.jobsToShow,
            pageNum: 0
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.jobsToShow !== this.props.jobsToShow) {
            this.setState({
                jobsToShow: nextProps.jobsToShow,
                pageNum: 0
            });
        }
    }

    toCards = (jobs) => {
        let idx = 0;
        const res = [];
        for (let job of jobs) {
            res.push(this.toJobCard(job, idx++));
        }
        return res;
    };

    toJobCard = (job, idx) => {
        return (
            <div className="job-card" key={idx}>
                <div>
                    <div className="company-logo">
                        <Avatar size={70} icon="twitter"/>
                    </div>
                    <div className="job-details">
                        <h2>{job.title}</h2>
                        <div>{job.companyname}</div>
                        <div><FaMapMarker/>{job.joblocation}</div>
                    </div>
                    <div className={"to-job-site"}>
                        <Button type={"primary"} href={job.joburl}>
                            To job site
                        </Button>
                    </div>
                    {CLEAR_FLOAT}
                </div>
                <Collapse>
                    <Panel header={"Show description"} key={1}>
                        <span dangerouslySetInnerHTML={{__html: job.description}}/>
                    </Panel>
                </Collapse>
            </div>
        );
    };

    pagination() {
        const pageCount = Math.ceil(this.state.jobsToShow.length / PER_PAGE);
        const {pageNum} = this.state;
        return (
            <div className={"center"}>
                <ReactPaginate
                    previousLabel={pageNum === 0 ? '' : 'prev'}
                    nextLabel={pageNum !== pageCount - 1 ? 'next' : ''}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={3}
                    forcePage={pageNum}
                    onPageChange={this.onPageChange}
                    containerClassName={'pagination'}
                    subContainerClassName={'pages pagination'}
                    activeClassName={'active'}
                />
                {CLEAR_FLOAT}
            </div>
        );
    }

    onPageChange = data => {
        this.setState({pageNum: data.selected});
    };

    render() {
        const {jobsToShow, pageNum} = this.state;
        if (jobsToShow.length <= PER_PAGE) {
            return (
                this.toCards(jobsToShow)
            );
        }
        const from = pageNum * PER_PAGE;
        const pageJobs = jobsToShow && jobsToShow.length>0  ?jobsToShow.slice(from, from + PER_PAGE):[];
        return (
            <div>
                {this.pagination()}
                <div>
                    {this.toCards(pageJobs)}
                </div>
                {this.pagination()}
            </div>
        );
    }
}
