import React from 'react';
import {Checkbox} from 'antd';

export function CheckBoxFilter({labels = [], selected = [], onChange, header}) {
    const shallowSelected = [...selected];

    const updateSelected = (label, added) => {
      if (!!added) {
          shallowSelected.push(label);
          onChange(shallowSelected);
      } else {
          onChange(shallowSelected.filter(el => el !== label));
      }
    };

    const toCheckBox = (label, checked) => {
        return (
            <div key={label}>
                <Checkbox onChange={() => updateSelected(label, !checked)} checked={checked}>{label}</Checkbox>
            </div>
        )
    };
    return (
        <div className={"checkbox-filter"}>
            <h3>{header}</h3>
            {labels.map(label => toCheckBox(label, selected.indexOf(label) > -1))}
        </div>
    )
}