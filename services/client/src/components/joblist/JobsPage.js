import React, {Component} from 'react';
import {Collapse, Input, Layout} from 'antd';
import * as selectors from '../../reduxsaga/mainselectors';
import connect from "react-redux/es/connect/connect";
import "../../styles/main.css"
import {CheckBoxFilter} from "./CheckBoxFilter";
import {isEmpty} from "lodash";
import {CLEAR_FLOAT} from "../../actions";
import {JobList} from "./JobsList";

const {Search} = Input;
const {Content} = Layout;
const {Panel} = Collapse;

class JobsPage extends Component {
    state = {
        filter: {
            freeText: '',
            companies: [],
            locations: []
        },
    };

    filterChanged = (field, newVal) => {
        const filter = {...this.state.filter};
        filter[field] = newVal;
        this.setState({filter});
    };

    render() {
        const {locations, companies} = this.props.dictionaries;
        const {filter} = this.state;

        let jobsToShow, jobCountString;
        const noFilter = Object.keys(filter).every(key => isEmpty(filter[key]));
        if (noFilter) {
            jobsToShow = this.props.jobs;
            jobCountString = `Found ${jobsToShow ? jobsToShow.length:[]} jobs in ${companies ? companies.length:[]} companies`;
        } else {
            const filteredLocations = new Set(filter.locations), filteredCompanies = new Set(filter.companies);

            const containsIgnoreCase = (str, target) => str.toLowerCase().includes(target.toLowerCase());

            const and = (fun1, fun2) => (x) => fun1(x) && fun2(x);
            const textPredicate = (job) => isEmpty(filter.freeText) ||
                                            containsIgnoreCase(job.title, filter.freeText) ||
                                            containsIgnoreCase(job.description, filter.freeText);

            const companiesPredicate = (job) => isEmpty(filteredCompanies) || filteredCompanies.has(job.companyname);
            const locationsPredicate = (job) => isEmpty(filteredLocations) || filteredLocations.has(job.joblocation);

            let filterPredicate = and(textPredicate, and(companiesPredicate, locationsPredicate));

            jobsToShow = this.props.jobs.filter(filterPredicate);
            jobCountString = `Filtered ${jobsToShow.length} of ${this.props.jobs.length} jobs`;
        }

        return (
            <Layout>
                <Content>
                    <div>
                        <Search className={"free-search"}
                                placeholder={"Free text search"}
                                value={filter.freeText}
                                onChange={(e) => this.filterChanged("freeText", e.target.value)}/>
                    </div>
                    <div>
                        <h4>{jobCountString}</h4>
                        <div className={"jobs-cards"}>
                            <JobList jobsToShow={jobsToShow}/>
                        </div>
                        <div className={"jobs-filters"}>
                            <CheckBoxFilter
                                header={"Companies"}
                                labels={companies}
                                selected={[...filter.companies]}
                                onChange={(selected) => this.filterChanged("companies", selected)}/>
                            <CheckBoxFilter
                                header={"Locations"}
                                labels={locations}
                                selected={[...filter.locations]}
                                onChange={(selected) => this.filterChanged("locations", selected)}/>
                        </div>
                        {CLEAR_FLOAT}
                    </div>
                </Content>
            </Layout>
        );
    }
}

const mapStateToProps = state => {
    return {
        started: selectors.ifAppStarted(state),
        jobs: selectors.AllJobs(state),
        filteredJobs: state.filteredJobs,
        dictionaries: state.dictionaries
    }
};

export default connect(mapStateToProps, null)(JobsPage)
