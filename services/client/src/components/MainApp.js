import React, {Component} from 'react';
import {Icon, Layout, Menu} from 'antd';
import * as selectors from '../reduxsaga/mainselectors';
import connect from "react-redux/es/connect/connect";
import "../styles/main.css"

import JobList from "./joblist/JobsPage";
import {updateDictionaries} from "../actions";
import {isEmpty} from "lodash";

const {SubMenu} = Menu;
const {Header, Content, Footer} = Layout;

class MainApp extends Component {
    state = {
        current: 'jobs',
    };

    handleClick = e => {
        console.log('click ', e);
        this.setState({
            current: e.key,
        });
    };

    getMenu = () => {
        return (
            <Menu onClick={this.handleClick}
                      selectedKeys={[this.state.current]}
                      style={{ lineHeight: '64px' }}
                      mode="horizontal">
            <Menu.Item key="jobs">
                <Icon type="user"/> Jobs list
            </Menu.Item>
        </Menu>
        )
    };

    render() {
        if (isEmpty(this.props.dictionaries) && !isEmpty(this.props.jobs)) {
            this.props.updateDictionaries(this.props.jobs);
        }
        return (
            <Layout>
                <Header>
                    <div className="logo">
                        <img src={"https://gvahim.org.il/wp-content/uploads/2017/06/Gvahim-1.png"} height={50}/>
                    </div>
                    {this.getMenu()}
                </Header>
                <Content className="content">
                    <h2>Gvahim job board</h2>
                    <JobList/>
                </Content>
                <Footer className={"footer"}/>
            </Layout>
        );
    }
}

// export default MainApp;
const mapStateToProps = state => {
    return {
        started: selectors.ifAppStarted(state),
        jobs: selectors.AllJobs(state),
        dictionaries: state.dictionaries
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateDictionaries: updateDictionaries(dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainApp)
