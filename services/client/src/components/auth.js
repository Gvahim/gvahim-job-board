import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import React, {Component} from 'react';

import * as act from '../actions';
import * as selectors from '../reduxsaga/mainselectors';
import connect from "react-redux/es/connect/connect";

const uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
    signInSuccessUrl: '/signedIn',
    // We will display Google and Facebook as auth providers.
    signInOptions: [
        act.firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        act.firebase.auth.FacebookAuthProvider.PROVIDER_ID
    ]
};
class Auth extends Component {
    state = {
        current: 'mail',
    };

    handleClick = e => {
       // console.log('click ', e);
      //  this.setState({current: e.key,});
    };

    render() {

        return (<div><StyledFirebaseAuth firebaseAuth={act.firebase.auth()} uiConfig={uiConfig} /></div>
        );
    }
}


// export default MainApp;
const mapStateToProps = state => {
    return {
        started: selectors.ifAppStarted(state),
        jobs: selectors.AllJobs(state)


    }
};

export default connect(mapStateToProps, null)(Auth)
