class Services {
    constructor(db) {
        this.db = db;
    }
    async init() {
        console.log('async init');
    }
    async getLatestJob() {
        let sql = `SELECT DISTINCT ON (website_id) version, 
                   website_id,
                   id,
                   json_data->>'CompanyName' as CompanyName,
                   json_data->>'Title' as Title,
                   json_data->>'JobLocation' as JobLocation,
                   json_data->>'JobUrl' as JobUrl,
                   json_data->>'description' as description 
                   FROM crawler.parse_data
                   ORDER BY website_id, version desc`;
        sql=`SELECT DISTINCT ON (website_id) version, website_id,id,
                   json_array_length(json_data),
                   json_array_elements(json_data)->'title' as Title,
                   json_array_elements(json_data)->'job_url' as JobUrl,
                   json_array_elements(json_data)->'company_name' as CompanyName,
                   json_array_elements(json_data)->'job_location' as JobLocation,
                   json_array_elements(json_data)->'description' as description
FROM crawler.parse_data
ORDER BY website_id, version desc`;
        let result = await this.db.query(sql);
        if (result) return result.rows; else return false;
    }

    async generateReduxFile()
    {
        let jobs=await this.getLatestJob();
     //   console.log()
        let redux={data:{rt:19}, jobs}
        return redux;
    }
}
module.exports = {Services};

