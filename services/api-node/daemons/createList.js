
const connectionString = process.env.POSTGRES_LINE || 'postgres://test:test@localhost:5435/jobboard_gvahim';
const {connectToDb}=require('../db/')
const {Services}=require('../services');
const fs=require("fs");
// console.log('Debug', connectionString, process.env)

const doJob=async()=> {

    const db = await connectToDb(connectionString);
    const services=new Services(db);
    await services.init();
    const fileData= await services.generateReduxFile();
    fs.writeFileSync('jobs.json', JSON.stringify(fileData));

    //console.log(res);



    process.exit();

}

doJob();
