const {connectToDb} = require('./index.js');

const connectionString = process.env.POSTGRES_LINE || 'postgres://test:test@localhost:5435/jobboard_gvahim';

const initTables = []

const seeds=[];

const seedData=[]

const openCreate = async () => {
    try {
        const db = await connectToDb(connectionString);

        for (let line of initTables) {
            try {

                const name=line.substr(line.indexOf('EXISTS')+7, line.indexOf(' ',line.indexOf('EXISTS')+7))
                await db.query(line);
                console.log(name, 'created');


            }
            catch (ex) {
                console.log(`Error while creating ${line}`, ex)
            }
        }
        for (let line of seedData) {
            const {table, values}=line;
            const columnsLine=seeds.find((e)=>(e.table===table));
            if (columnsLine) {
                const {columns}=columnsLine;

                try {
                    const sq=`insert into ${table}(${columns})values(${values}) on conflict do nothing`;
                    console.log(sq);
                    await db.query(sq);

                }
                catch (ex) {
                    console.log(`Error while creating ${line}`, ex)
                }
            }
        }


        process.exit();
        // return db;
    }
    catch (ex) {
        return false;
    }
}


openCreate();
