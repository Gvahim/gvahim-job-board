// const koaBody = require('koa-body');
// const bodyParser = require('koa-bodyparser');
const Koa = require('koa');
const app = new Koa();
const jwt = require('koa-jwt');
const session = require('koa-session')
const LocalStrategy = require('passport-local').Strategy;
const koaBody = require('koa-body');
const fs = require('fs');
// const http2 = require('http2');
const http = require('http');
// const swagger = require('swagger-koa');
// const serve = require('koa-static')
const appPort = process.env.HTTP_PORT || 3000

class Controllers {

    constructor(services, db) {
        this.services = services;
        this.db = db;
        const router = require('koa-router')();
        router.post('/data/main', koaBody(), this.getMainInfo.bind(this));
        router.post('/data/login', koaBody(), this.login.bind(this));
        router.post("/data/logout", koaBody(), this.logout); // поменять на POST!
        router.post("/data/retrieve", koaBody(), this.retrieve); // поменять на POST!
        router.post('/data/register', koaBody(), this.register2.bind(this));
        router.get("/live", this.live);
        router.get("/testmain", this.mainInfo);
        router.get("/jobs", this.Jobs.bind(this));

        router.get("");
        // router.get("/login/:login/:password", this.login); // поменять на POST!
        //router.get("/register/:login/:password", this.register.bind(this)); // поменять на POST!
        const crouter = require('koa-router')();
        crouter.get("/test", this.mainInfo);
        this.crouter = crouter;

        this.router = router;

        // this.register=this.register.bind(this);
    }

   /* async register(ctx) {
        try {
            const {login, password} = ctx.params;
            let res = await this.services.regiserUser({login, password});
            console.log('regres', res);
            if (res && res.result) {
                ctx.body = JSON.stringify({result: 'ok', data: res.result.id});
            } else {
                ctx.body = JSON.stringify({result: 'err', reason: res.reason ? res.reason : ''});
            }
        }
        catch (ex) {
            console.log('register', ex);
            ctx.body = JSON.stringify({result: 'err'})
        }
    }*/

    async logout(ctx) {
        await ctx.logout();
        this.services.logoutService();
        ctx.body = JSON.stringify({result: 'ok'});
    }

    async retrieve(ctx) {
        await ctx.logout();
        this.services.retrieveService();
        ctx.body = JSON.stringify({result: 'ok'});
    }

    async startUse(ctx, next) {
        console.log('debug:', ctx.request.url, ctx.request.method, ctx.request.header['content-type'] || "");

        // ctx.status = 200
        // ctx.type = "application/json";
        /* DEBUG MODE! */
        if (ctx.request.url.indexOf('acme-challenge') >= 0) {
            //console.log('inside qcme');
            ctx.status = 200;
            if (ctx.request.url.indexOf('7QO9p7fit62BM7b0sxmgG5Bn5jelyKZ7kQpjKPvOoBc') >= 0) ctx.body = '7QO9p7fit62BM7b0sxmgG5Bn5jelyKZ7kQpjKPvOoBc.aDG3fmtZs1gmCITZv074nJ9l0PcyugUAi0IBI5oao6g';
            else ctx.body = `XassTcGjarYAJNlvNkNcI1GF-mC_3EYNmfB4TXgRNec.aDG3fmtZs1gmCITZv074nJ9l0PcyugUAi0IBI5oao6g`;

        }
        if (ctx.request.url.indexOf('live') >= 0) {
            ctx.status = 200;
            ctx.body = JSON.stringify({status: 'success'});
        }
        if (ctx.request.method === 'OPTIONS') {
            ctx.status = 200;
            ctx.body = 'OK';
        }
        ctx.set('Access-Control-Allow-Origin', '*');
        ctx.set('Access-Control-Allow-Headers', '*');
        ctx.set('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
        /* DEBUG MODE! */
        // console.log('Setting status',   ctx.request.method,  ctx.request.url);
        // Call the next middleware, wait for it to complete
        await next()
    }

    start() {
                app.use(this.startUse);
                        app.keys = ['secret']
                        app.use(session({}, app));
                        const passport = require('koa-passport');
                        const options = {};
                        passport.serializeUser(async (user1, done) => {
                            console.log('Inside serializeUser');
                            const user = await this.services.checkLogin(user1);
                            console.log('found data', user);
                            if (user) {
                                console.log('done login', user);
                                this.services.saveLogin(user);
                                // ctx.state.userid=user.id;
                                done(null, {user});
                            }
                            else {
                                done('user not found', null);
                            }
                        });
                        passport.deserializeUser((user, done) => {
                            console.log('Inside deserializeUser', user);
                            done(null, user);

                        });
                        passport.use(new LocalStrategy(options, (username, password, done) => {
                            console.log('LocalStrategy', username, password, done);
                            // done({username});
                        }));

                        app.use(passport.initialize())
                        app.use(passport.session());

                        app.use(this.router.routes());

        // app.use(jwt({secret: 'shared-secret'}));
        // app.use(this.crouter.routes());

        if (process.env.USER && process.env.USER.indexOf && process.env.USER.indexOf('zienko') >= 0) {
            console.log('started dev',4000);
            http.createServer(app.callback()).listen(4000);
        }
        else {
            console.log('Listening...', appPort);

            http.createServer(app.callback()).listen(appPort);
        }
    }


    async login(ctx) {
        console.log('login controller', ctx.request.body, new Date().getTime());
        try {
            const {username, password} = ctx.request.body;
            const res2 = await ctx.login({login: username, password});
            console.log('login result', ctx.state, this.services.activeUser);
            const {id, login} = this.services.activeUser;
            //  this.services.saveLogin({id, login});
            ctx.body = JSON.stringify({result: 'ok', user: {id, login}});
        }
        catch (ex) {
            console.log('login err', ex)
            ctx.body = JSON.stringify({error: 'no user'});
        }
    }




    async register2(ctx) {
        console.log('register', ctx.request.body, new Date().getTime());
        try {
            const {email, password, nickname, phone} = ctx.request.body;
            let res = await this.services.regiserUser({login: email, password, nickname, phone});
            console.log('regres', res);
            if (res && res.result) {
                ctx.body = JSON.stringify({result: 'ok', data: res.result.id});
            } else {
                ctx.body = JSON.stringify({result: 'err', reason: res.reason ? res.reason : ''});
            }
        }
        catch (ex) {
            console.log('register', ex);
            ctx.body = JSON.stringify({result: 'err'})
        }
    }

    async getMainInfo(ctx) {
        console.log('getMainInfo', new Date().getTime());
        const txts = await this.services.getTexts();
        const categories = await this.services.getTask_Categories();
        //const blockchains = await this.services.getBlockchains();
        //const categories = await this.services.getAppCategories();
        const res = {result:{txts, categories}} ;
        // console.log('found', res);
        ctx.type = "application/json";
        ctx.body = JSON.stringify(res);
    }


    async live(ctx) {
        //onsole.log('Checking liveness', ctx);
        ctx.status = 200;
        ctx.body = JSON.stringify({status: 'success'});
    }



    async Jobs(ctx) {
        const jobs=await this.services.generateReduxFile();
        ctx.status = 200;

        ctx.body="   window.__PRELOADED_STATE__ ="+JSON.stringify(jobs);

    }
    async mainInfo(ctx) {
        console.log('mainInfo', ctx.state);
        console.log(ctx.request.url);
        // console.log(ctx.isAuthenticated());
        if (ctx && ctx.isAuthenticated && ctx.isAuthenticated()) {
            // ctx.body = 'Hello, ' +  + ' !';
            ctx.type = "application/json";
            ctx.body = JSON.stringify({"User": ctx.state.user.user.login});
        }
        else {
            //ctx.type = "application/json";
            // ctx.body = JSON.stringify({"User": "Unauth"});
            try {


                ctx.type = 'text/html';
                const rs = '<div>A</div>'; //fs.readFileSync('../client/build/index.html');
                ctx.body = rs;
            }
            catch (ex) {
                console.log('mainInfo', ctx);
                ctx.status = 404;

            }
        }
    }
}

module.exports = {Controllers}
