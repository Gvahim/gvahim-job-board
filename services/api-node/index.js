global.Promise = require('bluebird');
const {connectToDb} = require('./db');
const {Controllers}=require('./controllers');
const {Services}=require('./services');
// const cluster = require('cluster');


const connectionString = process.env.POSTGRES_LINE || 'postgres://test:test@localhost:5435/jobboard_gvahim';
// console.log('connectionString', connectionString);
const start = async () => {
    try {
        const db =  await connectToDb(connectionString);
        const services=new Services(db);
        await services.init();
        const router=new Controllers(services, db);
        router.start();
    }
    catch (ex) {
        console.log('Error',ex);
        process.exit();
        return false;
    }
}

start();

