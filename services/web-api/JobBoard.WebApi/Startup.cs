using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobBoard.WebApi.Dal;
using JobBoard.WebApi.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace JobBoard.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            
            // Add OpenAPI/Swagger document
            services.AddOpenApiDocument(document => // registers a OpenAPI v3.0 document with the name "v1" (default)
            {
                document.PostProcess = d => { d.Info.Title = "Gvahim Job Board API"; };
            });
            
            services.RegisterDbRepository();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseRouting();
            
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            
            // Add OpenAPI/Swagger middlewares
            app.UseOpenApi(); // Serves the registered OpenAPI/Swagger documents by default on `/swagger/{documentName}/swagger.json`
            app.UseSwaggerUi3(); // Serves the Swagger UI 3 web ui to view the OpenAPI/Swagger documents by default on `/swagger`
        }
    }
    
    internal static class DbIoCExtensions
    {
        public static void RegisterDbRepository(this IServiceCollection services)
        {
            var host = Environment.GetEnvironmentVariable("POSTGRES_URL") ?? "localhost";
            var port = Environment.GetEnvironmentVariable("POSTGRES_PORT") ?? "5435";
            var user = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD") ?? "test";
            var pwd = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD") ?? "test";
                
            var connectionString = $"Server={host};Port={port};Database=service;User Id={user};Password={pwd};";
                
            var dbOptionsBuilder = new DbContextOptionsBuilder<JobsContext>();
            dbOptionsBuilder.UseNpgsql(connectionString);

            var factory = new JobsContextFactory(dbOptionsBuilder.Options);
            services.AddSingleton<IJobsContextFactory>(factory);
            services.AddSingleton<IJobsRepository, JobsRepository>();

            // fake data generation
            using var context = factory.CreateContext();
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            var units = CreateFakeJobs();
            context.Jobs.AddRange(units);
            context.SaveChanges();
        }

        private static List<Job> CreateFakeJobs()
        {
            var result = new List<Job>();

            var rand = new Random();
            for (int i = 0; i < 100; i++)
            {
                var id = i + 1;
                result.Add(new Job()
                {
                    Id = id,
                    CompanyLogoUrl = "https://gvahim.org.il/wp-content/uploads/2017/06/Gvahim-1.png",
                    CompanyName = $"Test Company Name {id}",
                    CompanyUrl = "https://gvahim.org.il/",
                    JobLocation = $"Location of job {id}",
                    DetailedDescription = $"Description of job {id}",
                    Seniority = (JobSeniority)rand.Next(1, 5),
                    Type = (JobType)rand.Next(1, 14)
                });
            }
            
            return result;
        }
    }
}