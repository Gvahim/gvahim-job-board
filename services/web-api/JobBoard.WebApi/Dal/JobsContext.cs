using JobBoard.WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace JobBoard.WebApi.Dal
{
    public class JobsContext : DbContext
    {
        public JobsContext(DbContextOptions<JobsContext> options) : base(options){}
        
        public DbSet<Job> Jobs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Job>()
                .ToTable("Jobs")
                .HasKey(u => u.Id);
        }
    }
}