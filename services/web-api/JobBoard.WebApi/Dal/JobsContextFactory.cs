using System;
using Microsoft.EntityFrameworkCore;

namespace JobBoard.WebApi.Dal
{
    public interface IJobsContextFactory
    {
        JobsContext CreateContext();
    }
    
    public class JobsContextFactory : IJobsContextFactory
    {
        private readonly DbContextOptions<JobsContext> _options;

        public JobsContextFactory(DbContextOptions<JobsContext> options)
        {
            if(options == null) throw new ArgumentNullException(nameof(options));
            
            _options = options;
        }

        public JobsContext CreateContext()
        {
            return new JobsContext(_options);
        }
    }
}