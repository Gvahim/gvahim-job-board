using System;
using System.Collections.Generic;
using System.Linq;
using JobBoard.WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace JobBoard.WebApi.Dal
{
    public class JobsRepository : IJobsRepository
    {
        private readonly IJobsContextFactory _contextFactory;

        public JobsRepository(IJobsContextFactory contextFactory)
        {
            if(contextFactory == null) throw new ArgumentNullException(nameof(contextFactory));
            
            _contextFactory = contextFactory;
        }

        public Job GetJob(long unitId)
        {
            using (var context = _contextFactory.CreateContext())
            {
                return context.Jobs
                    .AsNoTracking()
                    .FirstOrDefault(u => u.Id == unitId);
            }
        }

        public ICollection<Job> GetAllJobs()
        {
            using (var context = _contextFactory.CreateContext())
            {
                return context.Jobs
                    .AsNoTracking()
                    .ToArray();
            }
        }
    }
}