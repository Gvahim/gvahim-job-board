using System.Collections;
using System.Collections.Generic;
using JobBoard.WebApi.Models;

namespace JobBoard.WebApi.Dal
{
    public interface IJobsRepository
    {
        ICollection<Job> GetAllJobs();

        Job GetJob(long jobId);
    }
}