using System;

namespace JobBoard.WebApi.Models
{
    /// <summary>
    /// Represent open position for show in datagrid
    /// </summary>
    public class JobPageElement
    {
        public long Id { get; set; }
        
        public string Title { get; set; } = String.Empty;
        
        public string CompanyName { get; set; } = String.Empty;
        
        public string CompanyUrl { get; set; } = String.Empty;
        
        public string CompanyLogoUrl { get; set; } = String.Empty;
        
        public string JobLocation { get; set; } = String.Empty;
        
        public string[] TechnologiesStack { get; set; } = new string[0];
        
        public JobType Type { get; set; } // 
        
        public JobSeniority Seniority { get; set; }
    }
}