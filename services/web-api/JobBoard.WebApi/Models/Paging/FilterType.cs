namespace JobBoard.WebApi.Models.Paging
{
    public enum FilterType
    {
        Unknown = 0,
        Equal = 1,
        NotEqual = 2,
        Contains = 3,
        InRange = 4
    }
}