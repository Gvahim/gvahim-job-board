namespace JobBoard.WebApi.Models.Paging
{
    /// <summary>
    /// Filtration parameters
    /// </summary>
    public class FiltrationParams
    {
        public FilterType FilterType { get; set; }
        
        /// <summary>
        /// Filter values. Should contains one element in case of <see cref="FilterType.Equal"/>, <see cref="FilterType.NotEqual"/>, <see cref="FilterType.Contains"/>.
        /// </summary>
        public string[]? FilterValues { get; set; }
    }
}