namespace JobBoard.WebApi.Models.Paging
{
    public enum SortingDirection
    {
        Ascending = 0,
        Descending = 1
    }
}