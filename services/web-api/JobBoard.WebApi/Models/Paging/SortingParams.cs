using System;

namespace JobBoard.WebApi.Models.Paging
{
    public class SortingParams
    {
        public string SortingField { get; set; } = String.Empty;
        
        public SortingDirection Direction { get; set; }
    }
}