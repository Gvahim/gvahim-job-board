namespace JobBoard.WebApi.Models.Paging
{
    public class Page<TElement>
        where TElement : class
    {
        public TElement[] PageElements { get; set; } = new TElement[0];
        
        public long PageNumber { get; set; }
        
        public long PageCount { get; set; }
    }
}