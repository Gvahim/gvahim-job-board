using JobBoard.WebApi.Models.Paging;

namespace JobBoard.WebApi.Models
{
    public class PageRequest
    {
        public long PageNumber { get; set; }
        
        public long PageSize { get; set; }
        
        public FiltrationParams[]? FiltrationParams { get; set; }
        
        public SortingParams? SortingParams { get; set; }
    }
}