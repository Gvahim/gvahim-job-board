namespace JobBoard.WebApi.Models
{
    public enum JobSeniority
    {
        Unknown = 0,
        Intern = 1,
        Junior = 2,
        Middle = 3,
        Senior = 4,
        Principal = 5
    }
}