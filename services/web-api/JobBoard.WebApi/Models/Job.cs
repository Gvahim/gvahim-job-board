using System;

namespace JobBoard.WebApi.Models
{
    /// <summary>
    /// Open position with detailed description
    /// </summary>
    public class Job : JobPageElement
    {
        public string DetailedDescription { get; set; } = String.Empty;
    }
}