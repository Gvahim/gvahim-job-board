namespace JobBoard.WebApi.Models
{
    public enum JobType
    {
        Unknown = 0,
        BackEnd = 1,
        FrontEnd = 2,
        Mobile = 3,
        Desktop = 4,
        FullStack = 5,
        Dba = 6,
        DataScience = 7,
        BigData = 8,
        Qa = 9,
        DevOps = 10,
        Analyst = 11,
        Architect = 12,
        Support = 13,
        ProjectManagement = 14
    }
}