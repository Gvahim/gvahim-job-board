﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobBoard.WebApi.Dal;
using JobBoard.WebApi.Models;
using JobBoard.WebApi.Models.Paging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace JobBoard.WebApi.Controllers
{
    /// <summary>
    /// Main controller to get pages of available jobs + detailed description of job by id
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class JobsController : ControllerBase
    {
        private readonly ILogger<JobsController> _logger;
        private readonly IJobsRepository _jobsRepository;

        public JobsController(IJobsRepository jobsRepository, ILogger<JobsController> logger)
        {
            _jobsRepository = jobsRepository;
            _logger = logger;
        }

        /// <summary>
        /// Get page of open positions (with filtration and sorting)
        /// </summary>
        /// <param name="request"></param>
        /// <remarks>
        /// Get with body
        /// </remarks>
        /// <returns></returns>
        [HttpPost("page")]
        public ActionResult<Page<JobPageElement>> GetJobsAsync(PageRequest request)
        {
            var pajeContetnt = _jobsRepository
                .GetAllJobs()
                .Select(j => (JobPageElement) j)
                .ToArray();
            
            return Ok(new Page<JobPageElement>()
            {
                PageElements = pajeContetnt,
                PageCount = pajeContetnt.Count()
            });
        }

        /// <summary>
        /// Get job description by id
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        [HttpGet("{jobId}")]
        public ActionResult<Job> GetJobDetails(long jobId)
        {
            return Ok(_jobsRepository.GetJob(jobId));
        }
    }
}