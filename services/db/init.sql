CREATE DATABASE jobboard_gvahim;
\c jobboard_gvahim;
DROP SCHEMA public CASCADE;
CREATE SCHEMA crawler;
CREATE SCHEMA service;

CREATE TABLE crawler.parse_data (
    id serial PRIMARY KEY,
    website_id VARCHAR (128) NOT NULL,
    json_data JSON NOT NULL,
    version INTEGER DEFAULT 1 NOT NULL,
    created TIMESTAMP DEFAULT Now()
);
